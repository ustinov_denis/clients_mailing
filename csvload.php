<html>
<head>
    <meta charset="utf-8">
</head>
<body>
    <form method="post" enctype="multipart/form-data">
        <input type="file" name="fload">
        <br>
        <input type="submit" name="fload">
    </form>
</body>



<?php

if (
    !array_key_exists('fload', $_FILES) ||
    !array_key_exists('tmp_name', $_FILES['fload']) ||
    is_null($_FILES['fload']['tmp_name'] ||
    !file_exists($_FILES['fload']['tmp_name']))
) {
    exit;
}

require_once './src/kernel.php';

use App\Application;
use App\Config;

$app = new Application();

$linesPool = Config::get('get.step');

$clients = $app->getContainer('clients');
$clients->clear();
$file = $app->getContainer('file');

$i = 0;
foreach ($file->getLines('clients.csv') as $line) {
    $data[] = [
        'fio' => $line[0],
        'phone' => $line[1],
    ];
    echo $line[0] . " (" . $line[1] . ")<br>";
    $i++;
    if ($i == $linesPool) {
        $clients->add($data);
        $data = [];
        $i = 0;
    }
}

$clients->add($data);
