<?php

require_once './src/kernel.php';

use App\Application;
use App\Config;

$app = new Application();

$step =  Config::get('get.step');

$sms = $app->getContainer('sms');
$mailing = $app->getContainer('mailing');
$mailing->setMailing('testmail6');

$clients = $app->getContainer('clients');
$clientsCount = ceil($clients->count() / $step);

$i = 0;
while ($i < $clientsCount) {
    // берем группу клиентов
    $clientsList = $clients->get(offset:$i * $step, limit:$step);

    $i++;
    $clientIds = array_keys($clientsList);

    // делаем запрос, была ли отправка по этим клиентам из текущей рассылки
    $alreadySend = $mailing->getIn($clientIds);
    $alreadySendIds = [];
    foreach ($alreadySend as $sendClient) {
        $alreadySendIds[] = $sendClient['clients_id'];
    }

    // определяем, кому из этой группы клиентов еще не рассылали
    //$noSendEarlier =  array_diff($clientIds, $alreadySendIds);
    $noSendEarlier =  array_diff($clientIds, $alreadySendIds);

    // если такие есть, рассылаем им
    if (count($noSendEarlier) > 0) {
        // собственно рассылка
        $needSend = array_intersect_key($clientsList, array_flip($noSendEarlier));

        $sms->send($needSend);

        // фиксация факта рассылки в базе
        // если разослали, но не зафиксировали, то в следующий раз отправим повторно,
        // лучше так, чем ошибочно не отправить вообще
        $mailing->addClientsToMail($needSend);
    }
}
