<?php

namespace App;

use App\libs\DataBase;
use App\Repository\ClientsRepository;
use App\Service\File;
use App\Service\MailingRepository;
use App\Service\Sms;

class Application
{
    private array $container;

    public function __construct()
    {
        error_reporting(E_ALL);

        $this->makeContainer();

        return $this->getContainer();
    }

    private function makeContainer(): void
    {
        $this->container['dataBase'] = new DataBase();
        $this->container['clients'] = new ClientsRepository($this->container['dataBase']);
        $this->container['file'] = new File();
        $this->container['sms'] = new Sms();
        $this->container['mailing'] = new MailingRepository($this->container['dataBase']);
    }

    public function getContainer(string $service = null): mixed
    {
        if ($service == null) {
            return $this->container;
        }

        if (array_key_exists($service, $this->container)) {
            return $this->container[$service];
        }

        return null;
    }
}
