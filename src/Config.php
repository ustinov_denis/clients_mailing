<?php

namespace App;

class Config
{
    private static array $params = [
        'db.hostname' => 'localhost',
        'db.port' => '3306',
        'db.username' => 'root',
        'db.password' => 'root',
        'db.database' => 'bhelp',
        'db.charset' => 'utf8mb4',
        'get.step' => 2,
    ];

    public static function get(string $name): mixed
    {
        if (array_key_exists($name, self::$params)) {
            return self::$params[$name];
        }

        return false;
    }

    public static function set(string $name, mixed $value): void
    {
        self::$params[$name] = $value;
    }
}
