<?php

namespace App\libs;

use App\Config;
use mysqli;
use RuntimeException;

class DataBase
{
    private mysqli $connect;

    /*
     * список допустимых таблиц, для защиты от sql-инъекций
     */
    private static array $allowedTables = [];

    /*
     * список допустимых полей в таблицах, для защиты от sql-инъекций
     */
    private static array $allowedFields = [];

    public function __construct()
    {
        mysqli_report(MYSQLI_REPORT_OFF);
        $this->connect = new mysqli(
            Config::get('db.hostname'),
            Config::get('db.username'),
            Config::get('db.password'),
            Config::get('db.database'),
            Config::get('db.port')
        );

        if ($this->connect->connect_errno) {
            throw new RuntimeException('ошибка соединения mysqli: ' . $this->connect->connect_error);
        }

        $this->connect->set_charset(Config::get('db.charset'));
        if ($this->connect->errno) {
            throw new RuntimeException('ошибка mysqli: ' . $this->connect->error);
        }

        self::$allowedFields = $this->getFields();

        self::$allowedTables = $this->getTables();

        return $this->connect;
    }

    public function select(
        string $table,
        array $where = [],
        array $groupBy = [],
        array $orderBy = [],
        $offset = 0,
        $limit = null
    ): array {
        if (!in_array($table, self::$allowedTables)) {
            throw new \Exception('Неверное название таблицы');
        }

        $offsetSql = '';
        if (!is_null($offset) && $limit) {
            $offsetSql = " limit " . (int)$offset;
            $offsetSql = $offsetSql . "," . (int)$limit;
        }

        $whereSql = '';
        if ($where) {
            if (!$this->checkFields(array_keys($where))) {
                throw new \Exception('Недопустимое поле');
            }

            list($paramValue, $valTypes, $whereSql) = $this->getWhereSql($where);
            $whereSql = " where " . $whereSql;
        }

        if ($groupBy) {
            // TO DO
        }

        if ($orderBy) {
            // TO DO
        }

        $sql = "SELECT * FROM " . $table . $whereSql . $offsetSql;

        $query = $this->connect->prepare($sql);

        if ($where) {
            $query->bind_param($valTypes, ...$paramValue);
        }

        $query->execute();

        if ($query->errno) {
            throw new RuntimeException('ошибка mysqli: ' . $this->connect->error);
        }

        $result = $query->get_result();

        if (!$result) {
            return [];
        }

        return $this->formatResult($result);
    }

    public function insert(string $table, array $data): int|false
    {
        if (!in_array($table, self::$allowedTables)) {
            throw new \Exception('Неверное название таблицы');
        }

        $fields = $this->getInsertFields($data);

        $wildCardSubItem = [];
        foreach ($fields as $item) {
            $wildCardSubItem[] = '?';
        }
        $wildCardItem = "(" . implode(',', $wildCardSubItem) . ")";
        $fieldList = implode(',', $fields);
        list($insertData, $valTypes, $wildCardsLine) = $this->getInsertData($data, $wildCardItem);

        $sql = "INSERT INTO " . $table . "(" . $fieldList . ") VALUES " . $wildCardsLine;

        $query = $this->connect->prepare($sql);

        $query->bind_param($valTypes, ...$insertData);
        $result = $query->execute();

        if ($query->errno) {
            throw new RuntimeException('ошибка mysqli: ' . $this->connect->error);
        }

        if ($result) {
            return $query->insert_id;
        }

        return false;
    }

    public function count(string $table): int
    {
        if (!in_array($table, self::$allowedTables)) {
            throw new \Exception('Неверное название таблицы');
        }

        $sql = "SELECT count(*) as count FROM " . $table;
        $query = $this->connect->query($sql);

        $result = $query->fetch_assoc();

        return $result['count'];
    }

    public function truncate(string $table): bool
    {
        if (!in_array($table, self::$allowedTables)) {
            throw new \Exception('Неверное название таблицы');
        }

        $sql = "DELETE FROM " . $table;

        return $this->connect->query($sql);
    }

    /*
     * взять список существующих полей в таблицах, прочие поля будут отклоняться
     */
    private function getFields(): array
    {
        $sql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.columns where TABLE_SCHEMA = '"
            . Config::get('db.database')
            . "' group by COLUMN_NAME";
        $query = $this->connect->query($sql);

        $fields = [];
        while ($item = $query->fetch_array()) {
            $fields[] = $item['COLUMN_NAME'];
        }
        sort($fields);
        return $fields;
    }

    /*
     * взять список существующих таблиц, прочие таблицы будут отклоняться
     */
    private function getTables(): array
    {
        $sql = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.tables where TABLE_SCHEMA = '"
            . Config::get('db.database') . "'";
        $query = $this->connect->query($sql);

        $fields = [];
        while ($item = $query->fetch_array()) {
            $fields[] = $item['TABLE_NAME'];
        }

        return $fields;
    }

    /*
     * проверка, что переданные в метод поля таблиц существуют в таблицах
     */
    private function checkFields(array $fields): bool
    {
        $sumFields = array_unique(array_merge($fields, self::$allowedFields));
        sort($sumFields);

        return ($sumFields == self::$allowedFields);
    }

    /*
     * получение и проверка списка полей в переданных данных
     */
    private function getInsertFields(array $data): array
    {
        $fields = [];

        foreach ($data as $item) {
            if (count($fields) == 0) {
                $fields = array_keys($item);
                continue;
            }
            if ($fields !== array_keys($item)) {
                throw new \Exception('Неверный формат входных данных для вставки');
            }
        }

        if (!$this->checkFields($fields)) {
            throw new \Exception('Недопустимое поле');
        }

        return $fields;
    }

    /*
     * подготовка данных для вставки
     */
    private function getInsertData(array $data, string $wildCardItem): array
    {
        $insertData = [];
        $valTypes = '';
        $wildCards = [];

        foreach ($data as $line) {
            $wildCards[] = $wildCardItem;
            foreach ($line as $fName => $item) {
                $insertData[] = $item;
                if (is_int($item)) {
                    $valTypes .= 'i';
                }
                if (is_string($item)) {
                    $valTypes .= 's';
                }
            }
        }
        $wildCardsLine = implode(',', $wildCards);
        return array($insertData, $valTypes, $wildCardsLine);
    }

    /*
     * обработка полученного результата
     */
    private function formatResult(bool|\mysqli_result $result): array
    {
        $data = [];
        while ($item = $result->fetch_assoc()) {
            $data[$item['id']] = $item;
        }

        return $data;
    }

    /*
     * формирование условия where
     */
    private function getWhereSql(array $where): array
    {
        $whereSql = [];
        $valTypes = '';
        $paramValue = [];

        foreach ($where as $whereItem => $whereVal) {
            if (is_array($whereVal)) {
                $wildCardItem = [];
                foreach ($whereVal as $subItem) {
                    $valTypes .= 'i';
                    $wildCardItem[] = '?';
                }
                $whereSql[] = $whereItem . " in (" . implode(',', $wildCardItem) . ")";
                $paramValue = array_merge($paramValue, $whereVal);
            }
            if (is_string($whereVal)) {
                $valTypes .= 's';
                $whereSql[] = $whereItem . " LIKE ?";
                $paramValue[] = $whereVal;
            }
            if (is_int($whereVal)) {
                $valTypes .= 'i';
                $whereSql[] = $whereItem . " = ?";
                $paramValue[] = $whereVal;
            }
        }

        return array($paramValue, $valTypes, implode(' AND ', $whereSql));
    }
}
