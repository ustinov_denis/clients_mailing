<?php

namespace App\Repository;

use App\libs\DataBase;

abstract class BaseRepository
{
    protected string $table;

    protected DataBase $dataBase;

    public function get(int $offset = 0, int $limit = null): array // TO DO
    {
        return $this->dataBase->select($this->table, [], [], [], $offset, $limit);
    }

    public function add(array $data): int
    {
        return $this->dataBase->insert($this->table, $data);
    }

    public function count(): int
    {
        return $this->dataBase->count($this->table);
    }
}
