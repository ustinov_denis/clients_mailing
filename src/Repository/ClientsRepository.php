<?php

namespace App\Repository;

use App\libs\DataBase;

class ClientsRepository extends BaseRepository
{
    protected string $table = 'clients';

    public function __construct(DataBase $dataBase)
    {
        $this->dataBase = $dataBase;
    }

    public function clear(): bool
    {
        return $this->dataBase->truncate($this->table);
    }
}
