<?php

namespace App\Service;

use App\libs\DataBase;
use App\Repository\BaseRepository;

class MailingRepository extends BaseRepository
{
    private string $mailing;

    private int $mailingId = 0;

    protected string $table = 'mailing_clients';

    private string $listTable = 'mailing_list';

    public function __construct(DataBase $dataBase)
    {
        $this->dataBase = $dataBase;
    }

    /*
     * ставим ид рассылки
     */
    public function setMailing(string $mailing): void
    {
        $this->mailing = $mailing;
        $alreadyExist = $this->dataBase->select($this->listTable, ['mailing' => $mailing]);

        if (count($alreadyExist) > 0) {
            $this->mailingId = array_key_first($alreadyExist);
        } else {
            $this->mailingId = $this->dataBase->insert($this->listTable, [['mailing' => $mailing]]);
        }
    }

    public function getIn(array $in): array
    {
        return $this->dataBase->select($this->table, ['mailing_list_id' => $this->mailingId, 'clients_id' => $in]);
    }

    /*
     * добавляем клиентов, которым отослали рассылку
     */
    public function addClientsToMail(array $sendClients): int
    {
        $data = [];

        foreach ($sendClients as $client) {
            $data[] = [
                'mailing_list_id' => $this->mailingId,
                'clients_id' => $client['id'],
            ];
        }
        return $this->add($data);
    }
}
