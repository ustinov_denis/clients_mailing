<?php

namespace App\Service;

use App\Validators\CsvValidator;
use Exception;

class File
{
    public function getLines($file)
    {
        $handle = fopen($file, 'rb');

        if (!$handle) {
            throw new Exception();
        }

        while (!feof($handle)) {
            $line = fgetcsv($handle, null, ';');
            $line = CsvValidator::validate($line);
            yield $line;
        }

        fclose($handle);
    }
}
