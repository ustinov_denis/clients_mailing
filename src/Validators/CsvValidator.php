<?php

namespace App\Validators;

class CsvValidator
{
    public static function validate(array $line)
    {
        if (
            !preg_match("/^((\+\d{1,4})[\- ]?)?(\(?\d{2,4}\)?[\- ]?)?[\d\- ]{6,10}$/", $line[1]) ||
            !preg_match("/^[ А-Яа-яЁё]{1,}$/u", $line[0])
        ) {
            throw new \Exception('Неверный формат данных: ' . implode(', ', $line));
        }

        return $line;
    }
}
