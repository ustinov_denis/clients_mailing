<?php

require_once 'Libs\DataBase.php';
require_once 'Config.php';
require_once 'Application.php';
require_once 'Repository\BaseRepository.php';
require_once 'Repository\ClientsRepository.php';
require_once 'Repository\MailingRepository.php';
require_once 'Services\File.php';
require_once 'Services\Sms.php';
require_once 'Validators\CsvValidator.php';
